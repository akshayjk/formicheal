const express = require('express');
const router = express.Router();

const Auth_Route = require('./modules/authMod/index');

router.use('/api/auth', ...Auth_Route);

module.exports = router;