this is a guide to this API.

--> to get this project started.. clone the repo and hit 'npm install', the go 'npm run start:prod'.

--> the below are the routes and needed payload

	--to create a new account(SignUP) - http://localhost:3100/api/auth/signup
		- first_name
		- last_name(optional)
		- userid(mailID only)
		- password (3-8)

	-- to log in and obtain token(signin) - http://localhost:3100/api/auth/login
		- first_name
		- password

	-- to check the token () 		- http://localhost:3100/api/auth/token
		- 'x-access-token'
