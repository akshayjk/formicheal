const mongoose = require('mongoose');

const schema = mongoose.Schema;

const userSchema = new schema({
    first_name:{
        type:String,
        required:true
    },
    last_name:{
        type:String
    },
    userid:{
        type:String,
        required:true
    },
    hash_password:{
        type:String,
        required:true
    },
    created_date:{
        type:Date,
        default:Date.now()
    }
},{
    versionKey:false
});

const user = mongoose.model('user', userSchema);

module.exports = user;