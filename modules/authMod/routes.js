const express = require('express');
const { newUser,userLogin,tokenVerify } = require('./controller')

const router = express.Router();

router.route('/signup').post(newUser);
router.route('/login').post(userLogin);
router.route('/token').post(tokenVerify);

module.exports = router;