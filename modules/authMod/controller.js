const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const user = require('../../models/userModel');

async function newUser(req,res){

    //console.log(req.xop);

    let doubleCheck = await user.findOne({ userid: req.xop.userid });

    if(doubleCheck){

        return res.status(400).json({
            err:true,
            message:`user already exists`
        })

    } else {

        let hash = await bcrypt.hashSync(req.xop.password, Math.floor(Math.random()*10));

        let aUser = new user({
            'first_name':req.xop.firstname,
            'userid':req.xop.userid,
            'hash_password':hash,
            'created_date':Date.now()
        });

        if(req.xop.lastname) aUser['last_name'] = req.xop.lastname;

        let status = await aUser.save();

        if(status._id){
            
            return res.status(200).json({
                err:false,
                message:`User added; continue to login`
            })

        }

    }

    return res.status(400).json({
        err:true,
        message:`Sorry..`
    })

}

async function userLogin(req,res){

    //console.log(req.xop);

    let ourUser = await user.findOne({
        userid:req.xop.userid
    });

    if (!ourUser) {

        return res.status(400).json({
            err:true,
            message:`Sorry, No user is registered in this userID`
        });
    
    }

    let flag = bcrypt.compareSync(req.xop.password,ourUser.hash_password);

    if(flag){

        let tempData = {
            User : ourUser.userid,
            Date : ourUser.created_date
        };

        let token = jwt.sign(tempData, 'MichealJohn');

        if(token) {
            
            return res.status(200).json({
                err:false,
                message:`you are successfully logged in`,
                token: token
            });

        }

    }
        
    return res.status(200).json({
        err:false,
        message:`Invalid Credentials`
    });

}

async function tokenVerify( req,res) {

    try{

        let result = jwt.verify(req.xop.token, 'MichealJohn');

        if(result) {

            let userDetail = await user.findOne({
                userid : result.User,
                created_date : result.Date
            });

            if(userDetail) {
                return res.status(200).json({
                    err:false,
                    message:`Welcome ${userDetail.first_name}`
                });
            }

            return res.status(404).json({
                err:false,
                message:`User profile not active`
            })

        }

    } catch (e) {

        return res.status(404).json({
            err:true,
            message:`Token invalid`
        })
    }

    return res.status(200).json({
        err:false,
        message:`request reached`
    });

}

module.exports = {
    newUser, 
    userLogin,
    tokenVerify
}