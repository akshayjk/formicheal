const authGuard = require('./guard');
const authValidate = require('./validation');
const AuthRoutes = require('./routes');

const Auth_Route = [
    authGuard,
    authValidate,
    AuthRoutes
]

module.exports = Auth_Route ;