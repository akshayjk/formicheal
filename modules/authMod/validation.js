const express = require('express');
const joi = require('@hapi/joi');

const router = express.Router();

router.route('/signup').post(async function(req,res,next){
    
    let schema = joi.object({
        first_name:joi.string().required().min(2),
        last_name:joi.string().min(2),
        userid:joi.string().email().required(),
        password:joi.string().required().min(4).max(8),
    });

    try{
        let validatedPacket = await schema.validate(req.body);

        if(validatedPacket.error) {

            res.xvalidate = false;
            res.xerror = validatedPacket.error.details[0].message;
            next(validatedPacket.error);

        } else {
            
            req.xop ={
                firstname:validatedPacket.value.first_name,
                userid:validatedPacket.value.userid,
                password:validatedPacket.value.password
            }
    
            if(validatedPacket.value.last_name) {
                req.xop.lastname = validatedPacket.value.last_name;
            }

            res.xvalidate = true;
            next();

        }
    } catch(e){

        console.log('Server Crashed on Auth Validation');

    }

});

router.route('/login').post(async function(req,res,next){

    let schema = joi.object({
        userid:joi.string().email().required(),
        password:joi.string().required()
    });

    try {

        let validatedPacket = await schema.validate(req.body);

        if(validatedPacket.error) {
            
            res.xvalidate = false;
            res.xerror = validatedPacket.error.details[0].message;
            next(validatedPacket.error);

        } else{
            
            req.xop = {
                userid:validatedPacket.value.userid,
                password:validatedPacket.value.password
            }
            res.xvalidate= true;
            next();
        }

    } catch (e) {
        console.log(`server Crashed on auth login`)
    }

});

router.route('/token').post(function(req,res,next){
    
    let schema = joi.object({
        'x-access-token':joi.string().required()
    });

    try{
        
        //flag for you, Micheal
        let validatedPacket = schema.validate(req.body);

        if(validatedPacket.error) {
            
            res.xvalidate = false;
            res.xerror = validatedPacket.error.details[0].message;
            next(validatedPacket.error);

        } else {
            
            req.xop = {
                token: validatedPacket.value['x-access-token']
            };

            res.xvalidate = true;
            next();

        }

    } catch (e){

        console.log('Server crashed at token auth');

    }

});

module.exports = router;