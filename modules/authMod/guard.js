const express = require('express');
const router = express.Router();

router.route('/signup').post(function(req,res,next){
    
    res.xguard = true;
    next();

});

router.route('/login').post(function(req,res,next){
    
    res.xguard = true;
    next();

});

router.route('/token').post(function(req,res,next){
    
    res.xguard = true;
    next();

})

module.exports = router;