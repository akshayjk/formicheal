const app = require('express')();
const bp = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const router = require('./routes');

app.use(bp.json());
app.use(bp.urlencoded({extended:true}));
app.use(cors());

mongoose.connect('mongodb://localhost:27017/forMicheal', { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on('connected', () => console.log('MongoDB is properly connected') );
mongoose.connection.on('error', () => console.log('MongoDB is not connected') );

app.use('/', router);

const errorHandler = (err,req,res,next)=>{
    if(res.xguard == false || res.xvalidate == false) {
        if(res.xvalidate==false) {
            return res.status(400).json({
                err:true,
                message:`${res.xerror}`
            });
        }
        return res.status(400).json({
            err:true,
            message:`Error`
        });
    }
}

app.use(errorHandler);

app.listen(3100, (err)=>{
    if(err) console.log('server went down');
    else console.log('server working properly')
})